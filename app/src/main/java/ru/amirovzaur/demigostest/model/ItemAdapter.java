package ru.amirovzaur.demigostest.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;

import ru.amirovzaur.demigostest.R;
import ru.amirovzaur.demigostest.utils.Utils;

/**
 * Created by Zaur on 12.01.2016.
 */
public class ItemAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater lInflater;
    private ArrayList<Item> objects;

    public ItemAdapter(final Context context, final ArrayList<Item> items) {
        this.context = context;
        this.objects = items;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item, parent, false);
        }

        Item i = (Item) getItem(position);

        ((TextView) view.findViewById(R.id.item_by)).setText(i.getAuthor());
        ((TextView) view.findViewById(R.id.item_text)).setText(i.getDescription());
        ((TextView) view.findViewById(R.id.item_time)).setText(i.getDate());
        ((TextView) view.findViewById(R.id.item_title)).setText(i.getTitle());
        new DownloadImageTask((ImageView) view.findViewById(R.id.item_image))
                .execute(i.getImage());
        return view;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Utils.logE("Ошибка передачи изображения", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
