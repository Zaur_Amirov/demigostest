package ru.amirovzaur.demigostest.model;

/**
 * Created by Zaur on 12.01.2016.
 */
public class Item {
    private String title;
    private String date;
    private String author;
    private String description;
    private String image;

    public Item(final String title, final String date, final String author, final String description, final String image) {
        this.title = title;
        this.date = date;
        this.author = author;
        this.description = description;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }


    public String getDate() {
        return date;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
