/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.amirovzaur.demigostest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ru.amirovzaur.demigostest.model.Item;
import ru.amirovzaur.demigostest.model.ItemAdapter;
import ru.amirovzaur.demigostest.tasks.DownloadRequest;
import ru.amirovzaur.demigostest.tasks.DownloadRequestCallback;
import ru.amirovzaur.demigostest.utils.C;
import ru.amirovzaur.demigostest.utils.Utils;

public class PageFragment extends Fragment implements DownloadRequestCallback {

    private static final String ARG_POSITION = "position";
    private ListView listView;
    private int position;

    public static PageFragment newInstance(int position) {
        PageFragment f = new PageFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        position = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (position == 2) {

            listView = (ListView) inflater.inflate(R.layout.top_position, container, false);

            DownloadRequest downloadRequest = new DownloadRequest(this);
            downloadRequest.execute();

            return listView;

        } else {

            LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

            FrameLayout fl = new FrameLayout(getActivity());
            fl.setLayoutParams(params);

            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources()
                    .getDisplayMetrics());

            TextView v = new TextView(getActivity());
            params.setMargins(margin, margin, margin, margin);
            v.setLayoutParams(params);
            v.setLayoutParams(params);
            v.setGravity(Gravity.CENTER);
            v.setBackgroundResource(R.drawable.background_card);
            v.setText("PAGE " + (position + 1));

            fl.addView(v);
            return fl;
        }
    }

    @Override
    public void error(final String errorMessage) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Utils.showToast(errorMessage);
            }
        });
    }

    @Override
    public void items(ArrayList<Item> list) {
        for (int i = 0; i < list.size(); i++) {
            Utils.logD("yhjuhygkjiu", C.TITLE + " = " + list.get(i).getTitle());
            Utils.logD("yhjuhygkjiu", C.DATA + " = " + list.get(i).getDate());
            Utils.logD("yhjuhygkjiu", C.AUTHOR + " = " + list.get(i).getAuthor());
            Utils.logD("yhjuhygkjiu", C.DESCRIPTION + " = " + list.get(i).getDescription());
            Utils.logD("yhjuhygkjiu", C.IMAGE + " = " + list.get(i).getImage());
        }

        final ItemAdapter adapter = new ItemAdapter(getActivity(), list);
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setAdapter(adapter);
            }
        });
    }
}
