package ru.amirovzaur.demigostest.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

import ru.amirovzaur.demigostest.App;
import ru.amirovzaur.demigostest.BuildConfig;
import ru.amirovzaur.demigostest.model.Item;

/**
 * Created by Zaur on 12.01.2016.
 */
public class Utils {

    private static final String TAG = "DemigosTest";

    /**
     * show toast
     *
     * @param message - your message
     */
    public static void showToast(final String message) {
        Toast toast = Toast.makeText(App.getInstance().getApplicationContext(),
                message,
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * Check network connection
     *
     * @return
     */
    public static boolean isNetworkEnabled() {
        final ConnectivityManager connMan = (ConnectivityManager) App
                .getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo info = connMan.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    /**
     * Get String by id
     *
     * @param id
     * @return
     */
    public static String getString(final int id) {
        return App.getInstance().getResources().getString(id);
    }

    /**
     * Log.d
     *
     * @param tag
     * @param msg
     */
    public static void logD(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Debug" : msg);
        }
    }

    /**
     * DBGE == Log errors(Log.e)
     *
     * @param tag
     * @param msg
     */
    public static void logE(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Error!" : msg);
        }
    }

    public static String request(final URL url) {
        String result = null;
        BufferedReader reader = null;
        DataOutputStream writer = null;


        try {
            // for https replace HttpURLConnection - HttpsURLConnection
            final HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();

            httpConnection.setRequestMethod("GET");

            httpConnection.setReadTimeout(C.TIMEOUT_SERVER_RESPONSE);


            // Get Response
            reader = new BufferedReader(new InputStreamReader(
                    httpConnection.getInputStream(), Charset.forName("UTF-8")));
            final StringBuffer response = new StringBuffer();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            Utils.logD(TAG, "resultJSON = " + response.toString());
            result = response.toString();
        } catch (IOException ex) {

            Utils.log(ex);
        } finally {
            Utils.close(reader);
            Utils.close(writer);
        }
        return result;
    }

    /**
     * Print exception stack trace
     *
     * @param error
     */
    public static void log(final Exception error) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Error: ", error);
        }
    }

    /**
     * @param obj - instance of {@link Closeable} to close gracefully
     */
    public static void close(final Closeable obj) {
        if (obj != null) {
            try {
                obj.close();
            } catch (IOException e) {
                Utils.logE(TAG, e.getMessage());
            }
        }
    }

    public static ArrayList<Item> parcingJsonItem(final JSONArray json) throws JSONException {
        ArrayList<Item> result = new ArrayList<Item>();

        for (int i = 0; i < json.length(); i++) {
            JSONObject object = json.getJSONObject(i);
            String title = object.getString(C.TITLE);
            String date = object.getString(C.DATE);
            String author = object.getString(C.AUTHOR);
            String description = object.getString(C.DESCRIPTION);
            String image = object.getString(C.IMAGE);

            result.add(new Item(title, date, author, description, image));
        }

        return result;
    }

}
