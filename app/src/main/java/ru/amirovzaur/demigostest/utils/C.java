package ru.amirovzaur.demigostest.utils;

/**
 * Created by Zaur on 12.01.2016.
 */
public class C {

    public static String HTTP_URL = "http://demigos.com/";
    public static String DOWNLOAD_TEST = "test/releases.html";
    public static int TIMEOUT_SERVER_RESPONSE = 20000;

    public final static String TITLE = "title";
    public final static String DATE = "date";
    public final static String AUTHOR = "author";
    public final static String DESCRIPTION = "description";
    public final static String IMAGE = "image";

    public final static String DATA = "data";
}
