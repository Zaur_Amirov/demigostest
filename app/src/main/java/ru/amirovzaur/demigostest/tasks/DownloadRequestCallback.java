package ru.amirovzaur.demigostest.tasks;

import java.util.ArrayList;

import ru.amirovzaur.demigostest.model.Item;

/**
 * Created by Zaur on 12.01.2016.
 */
public interface DownloadRequestCallback {
    void error(final String errorMessage);

    void items(final ArrayList<Item> list);
}
