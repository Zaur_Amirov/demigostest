package ru.amirovzaur.demigostest.tasks;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;

import ru.amirovzaur.demigostest.R;
import ru.amirovzaur.demigostest.utils.C;
import ru.amirovzaur.demigostest.utils.Utils;

/**
 * Created by Zaur on 12.01.2016.
 */
public class DownloadRequest extends AsyncTask {

    private final DownloadRequestCallback callback;
    private final String TAG = "DownloadRequest";

    public DownloadRequest(DownloadRequestCallback callback) {
        this.callback = callback;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        String response = null;
        URL url = null;
        try {
            url = new URL(C.HTTP_URL + C.DOWNLOAD_TEST);

            if (Utils.isNetworkEnabled()) {
                response = Utils.request(url);

                if (response == null || response.isEmpty()) {
                    callback.error(Utils.getString(R.string.noServerResponse));
                } else {
                    Utils.logD(TAG, response);
                    final JSONObject jsonObject = new JSONObject(response);
                    final JSONArray jsonOArray = jsonObject.getJSONArray(C.DATA);
                    callback.items(Utils.parcingJsonItem(jsonOArray));
                }
            } else
                callback.error(Utils.getString(R.string.noInternet));
        } catch (JSONException e) {

        }
        catch (MalformedURLException e) {

        }
        return null;
    }

}
